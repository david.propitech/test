# Extension Chrome ChatGPT pour la correction de texte 
(extension non empaquetée ou extension de développement)

# Installation :

|  |  |
|----------|----------|
| <h3>Téléchargez l'extension au format ZIP.</h3> | ![test](installation/1.png) |
| <h3>Décompressez le fichier ZIP et renommez le fichier 'formai-main.zip' en 'formai'.</h3> | ![test](installation/2.png) | 
| <h3>1. Copiez ou Tapez dans la barre d'URL (chrome://extensions/)</h3> <br><h3>2. Activez le mode développeur.</h3> <br>   <h3>3. Chargez l'extension non empaquetée.</h3> | ![test](installation/3.png) |
| <h3>Sélectionnez le dossier. | ![test](installation/6.png) |
| <h3>Cliquez sur l'icône extension dans la barre d'outils de Chrome et épingler.</h3> | ![test](installation/7.png) |


# Utilisation


|||
|--------|----------|
| <h3> 1. Cliquez sur l'icône FormAI en haut à droite. <br></h3> <h3> 2. Recherchez votre clé API et enregistrez-la. <br> </h3><h3> Ajoutez un prompt. <h3>|![test](installation/_1.png)|
| <h3> L'icône doit s'afficher dans tous les champs de texte, sauf dans les champs de mot de passe. </h3>|![test](installation/_2.png)|
| <h3> Vous pouvez sélectionner le texte, faire un clic droit, puis l'éditer avec FormAI.</h3> |![test](installation/_3.png)|
